<?php

include_once libraries_get_path('qiniu_storage_sdk') . '/autoload.php';
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class qiniuStorageController {
  protected $auth, $path;

  public function getAuth() {
    return $this->auth;
  }

  public function setAuth($auth) {
    $this->auth = $auth;
  }

  public function __construct() {
    $this->setAuth(new Auth(QINIU_ACCESS_KEY, QINIU_SECRET_KET));
  }

  /*
   *
   * Example:
   *  要上传文件的本地路径
   *  $filePath = './php-logo.png';
   *
   *  上传到七牛后保存的文件名
   *  $key = 'my-php-logo.png';
   */
  public function upload($filePath, $key) {
    // 要上传的空间
    $bucket = QINIU_BUCKET_NAME;

    // 生成上传 Token
    $token = $this->auth->uploadToken($bucket);

    // 初始化 UploadManager 对象并进行文件的上传。
    $uploadMgr = new UploadManager();
    list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
    if ($err !== NULL) {
      watchdog('Qiniu Upload Error', '<pre>' . check_plain(print_r($err, TRUE))
        . '</pre>', array(), WATCHDOG_DEBUG);
    }
    else {
      watchdog('Qiniu Upload Return', '<pre>' . check_plain(print_r($ret, TRUE))
        . '</pre>', array(), WATCHDOG_DEBUG);
    }
  }

  /**
   * Generate Images
   *
   * @param       $key
   * @param       $mode
   * Qiniu Image generate mode choose.
   *
   * @param array $options Image attributes
   * Examples: width, height, alt, title...
   *
   * @return string
   */
  public function image_generate($key, $mode = 2, $options = array()) {
    // Set Default Values
    $default = array(
      'width' => 0,
      'height' => 0,
      'alt' => '',
      'title' => ''
    );

    $options = array_merge($default, $options);
    $url = QINIU_GLOBAL_URL
      . $key . '?imageView2/' . $mode . '/w/' . $options['width'] . '/h/' . $options['height'];

    return '<img src="' . $url . '" alt="' . $options['alt'] . '" title="' . $options['title'] . '">';
  }

  public function file_url_generate($key, $options = array()) {
    return QINIU_GLOBAL_URL . $key;
  }

  public function update() {
  }

  public function delete() {
  }
}