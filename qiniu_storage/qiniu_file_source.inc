<?php

/**
 * @file
 * Provides a remote stream wrapper and filefield source integration.
 */

define('REMOTE_FILE_SOURCE_HINT_TEXT', 'http://example.com/files/file.zip');

/**
 * Implements hook_filefield_sources_info().
 */
function qiniu_storage_filefield_sources_info() {
  $source = array();
  $source['qiniu_storage'] = array(
    'name' => t('Qiniu Storage File'),
    'label' => t('Qiniu Storage File'),
    'description' => t('Link to a file located on a remote server.'),
    'process' => 'filefield_source_qiniu_storage_process',
    'value' => 'filefield_source_qiniu_storage_value',
    'weight' => 3,
  );
  return $source;
}

/**
 * Implements hook_theme().
 */
function qiniu_storage_theme() {
  return array(
    'filefield_source_qiniu_storage_element' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Theme the output of the autocomplete field.
 */
function theme_filefield_source_qiniu_storage_element($variables) {
  $element = $variables['element'];

  $element['url']['#suffix'] = drupal_render($element['upload']);
  return '<div class="filefield-source filefield-source-qiniu_storage clear-block">'
  . drupal_render($element['url']) . '</div>';
}

/**
 * A #process callback to extend the filefield_widget element type.
 */
function filefield_source_qiniu_storage_process($element, &$form_state, $form) {
  $element['filefield_qiniu_storage'] = array(
    '#weight' => 100.5,
    '#theme' => 'filefield_source_qiniu_storage_element',
    '#filefield_source' => TRUE,
    '#filefield_sources_hint_text' => REMOTE_FILE_SOURCE_HINT_TEXT,
  );

  $element['filefield_qiniu_storage']['url'] = array(
    '#name' => 'files[' . implode('_', $element['#parents']) . ']',
    '#type' => 'file',
    '#title' => t('Choose a file'),
    '#title_display' => 'invisible',
    '#size' => $element['#size'],
    '#theme_wrappers' => array(),
    '#weight' => -10,
  );

  $element['filefield_qiniu_storage']['upload'] = array(
    '#name' => implode('_', $element['#array_parents']) . '_select',
    '#type' => 'submit',
    '#value' => t('Upload'),
    '#validate' => array(),
    '#submit' => array('filefield_qiniu_storage_submit'),
    '#limit_validation_errors' => array($element['#parents']),
//    '#ajax' => array(
//      'path' => 'file/ajax/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value'],
//      'wrapper' => $element['#id'] . '-ajax-wrapper',
//      'effect' => 'fade',
//      'method' => 'replace',
//    ),
  );

  return $element;
}

/**
 * Upload File to qiniu storage server when user click upload.
 *
 * @param       $element
 * @param       $form_state
 * @param array $form
 */
function filefield_qiniu_storage_submit($element, &$form_state, $form = array()) {
  $node_language = $element['language']['#value'];
  $values = $form_state['values'];

  foreach ($values as $field_name => $field_values) {
    if (is_array($field_values)) {
      foreach ($field_values[$node_language] as $index => $file) {
        $file = file_load($file['fid']);
        if ($file) {
          $filePath = drupal_realpath($file->uri);
          $file_type = explode('/', $file->filemime);
          $key = $file->fid . '.' . $file_type[count($file_type) - 1];
          $qiniu_storage = new qiniuStorageController();
          $qiniu_storage->upload($filePath, $key);
        }
      }
    }
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * A #filefield_value_callback function.
 */
function filefield_source_qiniu_storage_value($element, &$item) {
  $file = file_load($item['fid']);
  if (!$file) {
    return;
  }
  $file_type = explode('/', $file->filemime);
  $key = $file->fid . '.' . $file_type[count($file_type) - 1];

  $qiniu_storage = new qiniuStorageController();
  $path = $qiniu_storage->file_url_generate($key);

  if ($path && $path != REMOTE_FILE_SOURCE_HINT_TEXT) {
    try {
      $file->uri = $path;
      $file->filename = $key;
      $file->filemime = file_get_mimetype($file->uri);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'user', 'user', $file->uid);
    }
    catch (Exception $e) {
      form_set_error('url', $e->getMessage());
      return;
    }

    if (empty($file->fid)) {
      form_set_error($element, t('Unable to add file from URL %file.', array('%file' => $value)));
      return;
    }

    // Run all the normal validations, minus file size restrictions.
    if (isset($element['#upload_validators']['file_validate_size'])) {
      unset($element['#upload_validators']['file_validate_size']);
    }

    if (filefield_sources_element_validate($element, (object) $file)) {
      $item = array_merge($item, (array) $file);
    }
  }
}